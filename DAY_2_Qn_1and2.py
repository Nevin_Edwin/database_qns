import sqlite3

conn = sqlite3.connect("Employee_database.db")
cur = conn.cursor()


def create():
    cur.execute("Drop Table IF EXISTS Employee")
    cur.execute("""
                    CREATE TABLE IF NOT EXISTS Employee(
                    Name CHAR(20),
                    ID integer,
                    Salary integer,
                    Department_id integer,
                    FOREIGN KEY (Department_id) REFERENCES Department (Department_id)
                    )
                """)
    conn.commit()


def add_col():
    cur.execute("ALTER TABLE Employee ADD COLUMN City CHAR(25)")
    conn.commit()


def insert_val(records):
    cur.executemany("INSERT INTO Employee(Name, ID, Salary, Department_id, City) Values(?,?,?,?,?)", records)
    conn.commit()


def show():
    cur.execute("SELECT Name, ID, Salary FROM Employee")
    item = cur.fetchall()
    print("\nd.\nName ID Salary ")
    for i in item:
        print(i[0], i[1], i[2])


def detail(col):
    cur.execute(f"SELECT {col} FROM Employee")
    flag = 0
    if col == 'Name':
        g = "First letter of name"
        name = [k[0] for k in cur.fetchall()]
    if col == "ID":
        g = "ID"
        name = cur.fetchall()

    user = input(f"For details about any employee please enter the {g}: ").title()
    print("\nName  ID  Salary  Department_id  City")
    for emp in name:
        if user == str(emp[0]):
            flag = 1
            if g == "ID":
                emp = int(emp[0])
            cur.execute(f"SELECT * FROM Employee WHERE {col}= '{emp}'")
            details = cur.fetchall()
            for v in details:
                print(v[0], v[1], v[2], v[3], v[4])
    if flag == 0:
        print("There is no employee with given details")


def change():
    cur.execute("SELECT ID FROM Employee")
    emp_id = cur.fetchall()
    try:
        user = int(input("Enter the ID of employee for changing Name: "))
        if user <= emp_id[-1][0]:
            name = input("Enter the new Name: ").title()
            cur.execute(f"UPDATE Employee SET Name='{name}' WHERE ID={user}")
            conn.commit()
            cur.execute(f"SELECT * FROM Employee WHERE Name='{name}'")
            for v in cur.fetchall():
                print(v[0], v[1], v[2], v[3], v[4])
        else:
            print("Invalid input...")
    except:
        print("Invalid input...")


record = [('Babu ', 1, 30000, 101, 'Trivandrum'), ('Julee', 2, 35000, 101, 'Kochi'),
          ('Binesh', 3, 25000, 102, 'Kollam'), ('Raju', 4, 50000, 105, 'Trivandrum'),
          ('sooraj', 5, 20000, 103, 'Kochi')]

print("\nQuestion 1:")
create()
print("\na. Successfully created the Table Employee which having 4 columns..")
add_col()
print("b. Successfully added new column City..")
insert_val(record)
print("c. Successfully Inserted the Records..")
show()
print("\ne.")
detail('Name')
print("\nf.")
detail('ID')
print("\ng.")
change()


def create_2():
    cur.execute("DROP TABLE IF EXISTS Department")
    cur.execute("""
                    CREATE TABLE Department(
                    Department_id integer PRIMARY KEY,
                    Department_name text)
                """)
    conn.commit()


def add_data(data):
    cur.executemany("INSERT INTO Department(Department_id, Department_name) Values(?,?)", data)
    conn.commit()


def more_details():
    try:
        user = int(input("\nc.\nEnter the Department id: "))
        cur.execute(f"""SELECT *
                    FROM Employee, Department 
                    WHERE Employee.Department_id={user} and Department.Department_id={user}
                    """)
        item = cur.fetchall()
        print("\nDepartment_id  Name  ID  Department_name  Salary  City")
        if len(item) == 0:
            print("There is no Department with given Department_id..")
        else:
            for items in item:
                print(items[3], items[0], items[1], items[6], items[2], items[4])
    except:
        print("Invalid input..")


datas = [(101, "Electronics"), (102, "Electrical"), (103, "Mechanical"), (104, "IT"), (105, "Civil")]

print("\nQuestion 2:")
create_2()
print("\na. Successfully created the Table Department which having 2 columns..")
add_data(datas)
print("b. Successfully Inserted the Records..")
more_details()

conn.close()
