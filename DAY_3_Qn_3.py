import sqlite3

conn = sqlite3.connect("Database.sqlite/database.sqlite")
cur = conn.cursor()


def print_short(data):
    for i in data:
        for j in range(len(i)):
            print(i[j], end="\t\t")
        print("")


def Query(col, table, condition):
    cur.execute(f"""
                    SELECT {col} FROM {table} {condition}
                """)
    return cur.fetchall()


print("\na.")
a_result = Query("HomeTeam, FTHG, FTAG", "Matches ", "WHERE Season=2010 and HomeTeam='Aachen' ORDER BY FTHG DESC")
print("HomeTeam\tFTHG\tFTAG")
print_short(a_result)

print("\nb. HomeTeam winning count in Season 2016: ")
b_result = Query(" COUNT(HomeTeam), HomeTeam", "Matches",
                 "WHERE Season=2016 and FTR='H' GROUP BY HomeTeam ORDER BY count(HomeTeam) DESC")
print("Count\tHomeTeam ")
print_short(b_result)

print("\nc. First 10 Rows from unique_Teams: ")
c_result = Query("unique_Team_ID, TeamName", "Unique_Teams", "LIMIT 10")
print("Team_id\tTeamName")
print_short(c_result)

print("\nd.Without JOIN Statement: ")
d_result_1 = Query("Match_ID,Unique_Teams.Unique_Team_ID, TeamName", "Teams_in_Matches, Unique_Teams",
                   "WHERE Teams_in_Matches.Unique_Team_ID = Unique_Teams.Unique_Team_ID LIMIT 50")
print("Match_ID Team_ID\tTeamName")
print_short(d_result_1)

print("\nd.With JOIN Statement: ")
d_result = Query("*", "Teams_in_Matches", "INNER JOIN Unique_Teams Using(Unique_Team_ID) LIMIT 50")
print("Match_ID Team_ID\tTeamName")
print_short(d_result)

print("\ne.")
e_result = Query("*", "Teams", "INNER JOIN Unique_Teams USING (TeamName) LIMIT 10")
print(
    "Season\t\tTeamName\t\tKaderHome\tAvgAgeHome\tForeignPlayersHome\tOverallMarketValueHome\tAvgMarketValueHome\tStadiumCapacity\t\tUnique_Team_ID")
print_short(e_result)

print("\nf.")
f_result = Query("Unique_Team_ID, TeamName, AvgAgeHome, ForeignPlayersHome", "Teams",
                 "INNER JOIN Unique_Teams USING (TeamName) LIMIT 5")
print("Team_ID\tTeamName\t\tAvgAgeHome\tForeignPlayersHome")
print_short(f_result)

print("\ng.")
g_result = Query("max(Match_ID), Unique_Team_ID, TeamName", "Teams_in_Matches",
                 "INNER JOIN Unique_Teams USING(Unique_Team_ID) WHERE substring(TeamName,-1,1)='y' or substring(TeamName,-1,1)='r' GROUP BY TeamName")
print("Match_ID\tTeam_ID\tTeamName")
print_short(g_result)

conn.commit()
conn.close()
