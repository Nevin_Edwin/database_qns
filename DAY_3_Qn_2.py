import sqlite3

conn = sqlite3.connect("Database.sqlite/database.sqlite")
cur = conn.cursor()


def Query(col, table, condition):
    cur.execute(f"""SELECT {col} From {table} {condition} """)
    return cur.fetchall()


print(f"\na. Count of all rows in Team table:")
a_result = Query("count(KaderHome)", "Teams", "")
print(a_result[0][0])

print(f"\nb. Unique value included in Season:")
b_result = Query('DISTINCT Season', "Teams", "")
for i in b_result:
    print(i[0])

print(f"\nc. Largest and Smallest Stadium capacity: ")
c_result = Query('max(StadiumCapacity), min(StadiumCapacity)', "Teams", '')
print(f"Largest Stadium Capacity: {c_result[0][0]}\nSmallest Stadium Capacity: {c_result[0][1]}")

print(f"\nd. Sum of Squad players for all team during 2014 Season: ")
d_result = Query('sum(KaderHome)', "Teams", " WHERE Season=2014")
print(d_result[0][0])

print(f"\ne. Man United score during home games on average: ")
e_result = Query("count(HomeTeam), sum(FTHG)", "Matches", "WHERE HomeTeam='Man United'")
print(e_result[0][1] / e_result[0][0])

conn.commit()
conn.close()
