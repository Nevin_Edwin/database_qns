import sqlite3

conn = sqlite3.connect("Cars.db")
cur = conn.cursor()
cur.execute("""drop table IF EXISTS Cars""")
cur.execute("""
                CREATE TABLE Cars (
                Car_Name CHAR(15) NOT NULL,
                Owner_Name CHAR(15))
            """)

for i in range(10):
    car_data = (input("Enter the name of car: "), input("Enter the name of owner: "))
    cur.execute("""
                INSERT INTO CARS(Car_Name, Owner_Name)
                VALUES(?,?)
            """, car_data)

cur.execute("""SELECT rowid,* FROM Cars""")

result = cur.fetchall()

print("\nCar_Name And its Owner_Name :")
for item in result:
    print(item[0], item[1], item[2])

conn.commit()
conn.close()
