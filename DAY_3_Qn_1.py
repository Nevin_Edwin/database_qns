import sqlite3

conn = sqlite3.connect("Database.sqlite/database.sqlite")
cur = conn.cursor()


def Query(col, condition):
    cur.execute(f"""
                    SELECT {col} FROM Matches 
                    WHERE {condition}
                """)
    item = cur.fetchall()
    for i in item:
        for j in range(len(i)):
            print(f"{i[j]}\t\t", end='')
        print("")


print('\nb. Teams played in season 2015 having FTHG 5\nHome Team\t\tAway Team')
Query('HomeTeam, AwayTeam', 'Season=2015 and FTHG=5')

print("\nc. Details of Matches where Arsenal in Home Team and FTR=A")
print("Match_ID\tDiv\t\tSeason\t\tDate\t\t\tHomeTeam\tAwayTeam\t\tFTHG\tFTAG\tFTR")
Query('*', "HomeTeam='Arsenal' and FTR='A'")

print("\nd. Details of Matches from 2012-2015 where Bayern Munich is in Away Team and FTAG>2")
print("Match_ID\tDiv\t\tSeason\t\tDate\t\t\tHomeTeam\tAwayTeam\t\t\tFTHG\tFTAG\tFTR")
Query("*", "Season>=2012 and Season<=2015 and  AwayTeam='Bayern Munich' and FTAG>2")

print("\nd. Details of Matches Where Home Team begins with A and Away Team begins with M")
print("Match_ID Div\tSeason\t\tDate\t\t\tHomeTeam\t\tAwayTeam\t\tFTHG\tFTAG\tFTR")
Query("*", "HomeTeam like 'A%' and AwayTeam like 'M%'")

conn.commit()
conn.close()
