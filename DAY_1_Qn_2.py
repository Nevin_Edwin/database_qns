import sqlite3

conn = sqlite3.connect("Hos_Doc_database.db")
cur = conn.cursor()
cur.execute("drop table if exists Hospital")
cur.execute("""
            CREATE TABLE Hospital(
            Hospital_id integer PRIMARY KEY,
            Hospital_Name text NOT NULL,
            'Bed Count' integer)
            """)

cur.execute("drop table IF EXISTS Doctor")
cur.execute("""
            CREATE TABLE Doctor(
            Doctor_id integer PRIMARY KEY,
            Doctor_Name text NOT NULL,
            Hospital_id integer,
            Joining_Date text NOT NULL,
            Speciality text NOT NULL,
            Salary integer,
            Experience,
            FOREIGN KEY (Hospital_id) REFERENCES Hospital (Hospital_id))
            """)

hos_data = [('Mayo Clinic', 200), ('Cleveland Clinic', 400), ('Johns Hopkins', 1000), ('UCLA Medical Center', 1500)]
cur.executemany("""
                    INSERT INTO 
                    Hospital(Hospital_Name, 'Bed Count')
                    Values(?,?)
                """, hos_data)

doc_data = [(101, 'David', 1, '2005-02-10', 'Pediatric', 40000), (102, 'Michael', 1, '2018-07-23', 'Oncologist', 20000),
            (103, 'Susan', 2, '20160-05-19', 'Garnacologist', 25000),
            (104, 'Robert', 2, '2017-12-28', 'Pediatric', 28000),
            (105, 'Linda', 3, '2004-06-04', 'Garnacologist', 42000),
            (106, 'William', 3, '2012-09-11', 'Dermatologist', 30000),
            (107, 'Richard', 4, '2014-08-21', 'Garnacologist', 32000),
            (108, 'karen', 4, '2011-10-17', 'Radiologist', 30000)]
cur.executemany("""
                    INSERT INTO
                    Doctor(Doctor_id, Doctor_Name, Hospital_id, Joining_Date, Speciality, Salary)
                    Values(?,?,?,?,?,?)
                """, doc_data)
conn.commit()


def find_doctor():
    flag_1 = 0
    flag_2 = 0
    cur.execute("""SELECT Speciality FROM Doctor""")
    list_spec = [k[0] for k in cur.fetchall()]
    comparator = [">", "<", ">=", "<=", "="]
    key_words = ["Above", "Below", "Greater Than Or Equal", "Lesser Than Or Equal", "Equal"]
    print(f"\nAvailable specialities are {list_spec}")
    while True:
        doc_spe = input("\nEnter the condition for selecting doctor. Eg:- Pediatric doctor with salary above 30000 :").title()
        for pos, spec in enumerate(list_spec):
            if spec in doc_spe:
                flag_1 = 1
                break
        if flag_1 == 1:
            for index, key in enumerate(key_words):
                if key in doc_spe:
                    cur.execute(
                        f"SELECT Doctor_Name, Salary FROM Doctor WHERE Speciality='{list_spec[pos]}' and Salary{comparator[index]}{int(doc_spe.split(' ')[-1])}")
                    item = cur.fetchall()
                    flag_2 = 1
                    break
        if flag_1 == 0 or flag_2 == 0:
            print("invalid input...")
            continue
        break
    return item


doc_result = find_doctor()
if len(doc_result) == 0:
    print("No Doctor available for the given data...")
else:
    print("\nCase 1: Doctors Available for given Condition and their salary :")
    for i in doc_result:
        print(i[0], i[1])
    print("\n")


def docWithHosp():
    cur.execute("""SELECT Doctor_Name, Hospital_Name FROM Doctor, Hospital 
                WHERE Doctor.Hospital_id = Hospital.Hospital_id""")
    print("Case 2: Doctors and their Hospitals:\n")
    for items in cur.fetchall():
        print(f"Dr.{items[0]} from {items[1]}")


docWithHosp()
conn.close()
